import styled from "styled-components"

export const Wrapper = styled.div`
    .category{  
        border-radius: 15px;
        border: none;
        box-shadow: 0 2px 8px #ddd;

        &__content{
            padding: 18px 0;
        }
        &__title{
            /* font-family: Inter; */
            font-size: 20px;
            font-weight: 500;
            line-height: 24px;  
        }
    }
`